# Lab 8 - Exploratory testing and UI

Website for tests: [Tinkoff.ru](https://www.tinkoff.ru)

## Test 1 (navigation bar titles)

Steps:

1. Go to [https://www.tinkoff.ru](https://www.tinkoff.ru)
2. Use *XPath* to get navigation bar elements.
3. Ensure that navigation bar element titles are present successfully.

## Test 2 (technical support phone)

Steps:

1. Go to [https://www.tinkoff.ru](https://www.tinkoff.ru)
2. Use *XPath* to get technical support label.
3. Ensure that the phone number is correct.

## Test 3 (login button label)

Steps:

1. Go to [https://www.tinkoff.ru](https://www.tinkoff.ru)
2. Use *XPath* to get login button label.
3. Ensure that login button leads to the logn page.
