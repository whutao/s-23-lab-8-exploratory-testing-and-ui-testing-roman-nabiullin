import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By

# Constants

TINKOFF_HOMEPAGE_URL: str = 'https://www.tinkoff.ru'

# Tests

class TinkoffRuHomepageTests(unittest.TestCase):

    def setUp(self) -> None:
        self.driver = webdriver.Firefox()
        self.driver.get(TINKOFF_HOMEPAGE_URL)

    def tearDown(self) -> None:
        self.driver.quit()

    def test_homepage_navigation_bar_titles(self) -> None:
        actual_navigation_bar = self.driver.find_elements(
            By.XPATH,
            '/html/body/div[1]/div/header/div/div/div/div/div[2]/div/nav/span'
        )
        actual_navigation_items = set(
            map(lambda item: item.text, actual_navigation_bar)
        )
        expected_navigation_items = set([
            'Кредитные карты',
            'Дебетовые карты',
            'Ипотека',
            'Вклады и счета',
            'Премиум',
            'Подписка',
            'Кредит наличными',
            'Автокредит',
            'Рефинансирование',
            ''
        ])
        self.assertSetEqual(actual_navigation_items, expected_navigation_items)

    def test_login_button_destination(self):
        login_button = self.driver.find_element(
            By.XPATH,
            '/html/body/div[1]/div/header/div/div/div/div/div[1]/div[3]/a'
        )
        login_button_destination = login_button.get_property('href')
        self.assertEqual(login_button_destination, TINKOFF_HOMEPAGE_URL + '/login/')

    def test_technical_support_phone_number(self):
        phone_label = self.driver.find_element(
            By.XPATH,
            '/html/body/div[1]/div/div[21]/footer/div/div[2]/div/div/div/a'
        )
        self.assertEqual(phone_label.text, '8 800 555-77-78')


if __name__ == '__main__':
    unittest.main()
